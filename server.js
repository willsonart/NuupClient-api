import express from 'express'
import bodyParser from 'body-parser'
import "dotenv/config"
import User from './routes/Users'
import Auth from './routes/Auth'

const app = express();

app.use(bodyParser.json())
app.use('/users',User)
app.use('/auth',Auth)

app.get('/', (req, res) => {
    res.send(`Bienvenido a la API de ${process.env.APPNAME}`);
});

app.use('*', (req, res) => {
    res.json('Not Found')
})

app.listen(process.env.PORT, function () {
  console.log(` API ${process.env.APPNAME} serving in port ${process.env.PORT} `);
});
