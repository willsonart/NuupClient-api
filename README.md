# API NuupClient

## Instalacion

### Descargar Aplicacion

Descargar el repositorio de gitlab

    git clone https://gitlab.com/willsonart/NuupClient-api.git nupp-api
    cd nupp-api

### Configuración

Una vez descargada la aplicacion debera configurar el archivo .env con la información correspondiente.

    cp .env.example .env


una vez configurado la aplicacion asegurese de tener la bd creada localmente

### Instalar ependencias

Debe instalar las dependencias con `yarn` o `npm` segun su preferencia ejecutando el siguiente comando.

    yarn install

## Corriendo el servidor

Para correr el servidor debe ejecutar el siguiente comando:

    yarn run dev

Se iniciara el servidor en modo prueba con nodemon.






