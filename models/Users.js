import knex from '../lib/knexFile'
import bookshelf from 'bookshelf'

const Bookshelf = bookshelf(knex)

const User = Bookshelf.Model.extend({
    tableName: 'MOCK_DATA'
});

export default User