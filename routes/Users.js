import express from 'express'
import User from '../models/Users'
import auth from '../lib/auth'
const router = express.Router()

router.use(auth.validToken)
router.use(auth.loginRequired)

router.get('/', async (req, res) => {
    const users = await User.fetchAll()
    if (!users) res.json('Not Found')
    res.json(users)
})

router.get('/:id', async (req, res) => {
    const user = await User.where({id: req.params.id}).fetch()
    if (!user) res.json('User Not Found')
    res.json(user)
})

router.post('/', async (req, res) => {
    const newUser = await new User({
                                id: req.body.id,
                                first_name: req.body.first_name,
                                last_name: req.body.last_name,
                                email: req.body.email,
                                password: req.body.password,
                                username: req.body.username
                            }).save(null, {method: 'insert'})
    
    if (!newUser) res.json('User Not Created')
    res.json(newUser)                  
})

router.put('/:id', async (req, res) => {

    const user = await new User({id: req.params.id})
    .save({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        password: req.body.password,
        username: req.body.username
    }, {patch: true})

    if (!user) res.json('User Not modified')
    res.json(user) 
})

router.delete('/:id', async (req, res) => {
    try {
        await new User({id: req.params.id}).destroy()
        res.json({
            success:true,
            message:`User.id = ${req.params.id} removed`
        })
    } catch (e){
        res.json({
            success:false,
            message:`User.id = ${req.params.id} not removed`
        })
    }
})

module.exports = router;