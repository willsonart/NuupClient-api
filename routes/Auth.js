import express from 'express'
import User from '../models/Users'
import auth from '../lib/auth'

const router = express.Router();

router.post('/login', async (req, res) => {
    const user = await User.where({
                                    email:req.body.email,
                                    password:req.body.password
                                }).fetch()

    if (!user) {
        res.json('User Not Found')
    } else {
        res.json({
            token : auth.getToken(user)
        })
    }

})

module.exports = router;