import jwt from 'jsonwebtoken'
import "dotenv/config"


const auth = {

    getToken:(user) => {
        const token = jwt.sign({
                                id: user.id,
                                first_name: user.first_name,
                                last_name: user.last_name,
                                email: user.email,
                                password: user.password,
                                username: user.Username
                            }, process.env.SECRET)
        if (token) return token
    },

    validToken:(req, res, next) => {
        if (req.headers && req.headers.authorization &&  req.headers.authorization.split(' ')[0] === 'Bearer') {
            const token = req.headers.authorization.split(' ')[1]
            jwt.verify(token, process.env.SECRET, (err, decode) => {
                if (err) req.user = null
                req.user = decode
            })            
        } else {
            req.user = null
        }
        next()
    },

    loginRequired : (req, res, next) => {
        if (req.user) {
          next();
        } else {
          return res.status(401).json({ message: 'Unauthorized user!' });
        }
    }
}

export default auth