
import knex from 'knex'
import "dotenv/config"

export default (knex)({
    client: 'mysql',
    connection: {
      host     : '127.0.0.1',
      user     : process.env.DBUser,
      password : process.env.DBPassword,
      database : process.env.DBDatabase,
      charset  : 'utf8'
    }
})
